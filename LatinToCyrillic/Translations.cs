﻿using System;
namespace LatinToCyrillic
{
    public static class Translations
    {
        public static string EnglishToCyrillic(string englishInput)
        {
            string workingCyrillicTranslation = englishInput;
            foreach (var charMap in MappingDictionaries.CyrillicLatinDict)
            {
                workingCyrillicTranslation = workingCyrillicTranslation.Replace(charMap.Value, charMap.Key);
            }
            return workingCyrillicTranslation;
        }
    }
}
