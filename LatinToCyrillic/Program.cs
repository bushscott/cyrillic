﻿using System;

namespace LatinToCyrillic
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter english text to be converted to Cyrillic.");
            string translatedText = Translations.EnglishToCyrillic(Console.ReadLine());
            Console.WriteLine("Your translated text is as follows.");
            Console.WriteLine(translatedText);
            Console.ReadLine();
        }
    }
}
